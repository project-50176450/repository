import React, { Component } from 'react';
import Routes from './Routes';

class App extends Component {
  render() {
    return (
      <div>
        <div className="main_content">
          <Routes />
        </div>
      </div>
    )
  }
}

export default App;

