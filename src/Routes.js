import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Home from './Component/Home';

class Routes extends Component {
    render() {
        return (
            <div>
                <BrowserRouter>
                    <Switch>
                        <Route path="/" exact={true} component={ Home } />
                    </Switch>
                </BrowserRouter>
            </div>
        )
    }
}

export default Routes;
