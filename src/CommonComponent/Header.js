import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import tringle_icon from '../Assets/images/tringle_icon.png';
import logo from '../Assets/images/logo.png';
import user_img from '../Assets/images/user_img.png';

class Header extends Component {
	constructor() {
        super();
        this.state = {
			isActive: false,
			isActiveHiW: false,
			isActiveUsr: false,
			isActiveHumger: false,
			user_id: 0,
			username: ''
        }
    }

	componentDidMount() {

		let user_id = localStorage.getItem("user_id")
		let username = localStorage.getItem("username")

		this.setState({
			user_id: user_id,
			username: username
		})

	}

	rctToggle = () => {
		this.setState({
			isActive: true
		})
	}

	rctToggleHiW = () => {
		this.setState({
			isActiveHiW: true
		})
	}

	rctToggleUsr = () => {
		this.setState({
			isActiveUsr: true
		})
	}

	rctToggleHumger = () => {
		this.setState({
			isActiveHumger: true
		})
	}

	render() {
		return (
			<div>
				<React.Fragment>
					<div className="fix_header wow fadeInDown animated" >
						<div id="header">
							<div className="doc_width">
								<div className="logo">
									<NavLink to="/" > <img src={logo} alt="" border="0" /> </NavLink>
								</div>
								<div className="header_right">
									<div className="login_menu">
										<li className="myaccount1" style={{ position: 'relative' }}>
											<NavLink className="tringle_img plus_icon" onClick={this.rctToggle} to="/home">All Auctions <img src={tringle_icon} alt="" border="0" /></NavLink>
											<div className={this.state.isActive ? 'menu-show slide_mayaccount slide_mayaccount_auction': 'slide_mayaccount slide_mayaccount_auction'} >
												<a href="auctions.html">Live Auctions</a>
												<a href="future.html">Upcoming Auctions</a>
												<a href="closed.html">Winners Auctions</a>
											</div>
										</li>
										<li><NavLink to="/packages">Buy Bids</NavLink></li>
										<li><NavLink to="/wheel">Spin Wheel</NavLink></li>
										<li className="menu_last myaccount1" style={{ position: 'relative' }}><NavLink className="tringle_img plus_icon"  to="" onClick={this.rctToggleHiW}>How Propenny Works <img src={tringle_icon} alt="" border="0" /></NavLink>
											<div className={this.state.isActiveHiW ? 'menu-show slide_mayaccount slide_mayaccount_how' : 'slide_mayaccount slide_mayaccount_how'}>
												<NavLink to="/penny_auction_guide">Penny Auctions Guide</NavLink>
												<NavLink to="/seat_auction_guide">Seat Auctions Guide</NavLink>
												<NavLink to="/unique_auction_guide">Unique Auction Guide</NavLink>
												<NavLink to="/ebay_auction_guide">Ebay Auction Guide</NavLink>
											</div>
										</li>
										{ this.state.user_id > 0 ?
											<React.Fragment>									
												<li className="welcomeuser" style={{ position: 'relative' }}><font>Hi,</font> <strong className="welcome_user myaccount" onClick={this.rctToggleUsr}>{this.state.username}</strong>
													<div id="slide_mayaccount_top" className={this.state.isActiveUsr ? 'menu-show slide_mayaccount slide_mayaccount_top': 'slide_mayaccount slide_mayaccount_top'}>
														<span className="top_tringle"></span>
														<span className="top_tringle2"></span>
														<div className="user_img">
															<img src={user_img} alt="" border="0"></img>
														</div>
														<div class="welcome_sec">
															<p><strong>{this.state.username}</strong></p>
															<p>demouser@gmail.com</p>
														</div>
														<div class="welcome_sec_btm">
															<a class="link_account" href="/users">My Account</a>
															<a class="logout_link" href="/logout">Logout</a>
														</div>	
													</div>
												</li>
											</React.Fragment>
											:
											<React.Fragment>
												<div className="right_link">
												<li>	
												<NavLink className="btn_new  left-animated btn-blue blue_fill" to="/login">Login</NavLink>
												<NavLink className="btn_new   left-animated right-animated btn-blue" to="/signup">Register</NavLink>
												</li>
											</div>
											</React.Fragment>
										}						
									</div>
								</div>
							</div>
						</div>
						<div class="main_menu mobile_none">
							<div class="doc_width">
								<a onClick={this.rctToggleHumger} className={this.state.isActiveHumger ? 'nav-btn nav-btn-close': 'nav-btn'} href="javascript:void(0)">&nbsp;</a>
 								<ul id="#navigation" className={this.state.isActiveHumger ? 'slide_login': 'slide_login closed'}>
									{
										this.state.user_id > 0 ?
										<React.Fragment>
										<li className="tasktop_non welcome_name"><NavLink  to="/users"> Hi, {this.state.username} </NavLink></li>
										<li><NavLink  to="/logout">Logout</NavLink></li>				
										</React.Fragment>
											:
										<React.Fragment>
										<NavLink to="/login">Login</NavLink>
										<NavLink className="btn_new   left-animated right-animated btn-blue" to="/signup">Register</NavLink>
										</React.Fragment>
									}
									<li className="dropdown" onClick={this.rctToggle} style={{ position: 'relative' }}><NavLink to="/home"> All Auctions <img className="dropdown-tringle" src={tringle_icon} alt="" border="0" /></NavLink>		
										<div className={this.state.isActive ? 'menu-show dropdown-menu': 'dropdown-menu'}>
											<a href="auctions.html">Live Auctions</a>
											<a href="future.html">Upcoming Auctions</a>
											<a href="closed.html">Winners Auctions</a>
										</div>
									</li>
									<li><NavLink to="/packages">Buy Bids</NavLink></li>
									<li><NavLink to="/wheel">Spin Wheel</NavLink></li>
									<li className="dropdown menu_last" onClick={this.rctToggleHiW}  style={{ position: 'relative' }}><NavLink to="">How Propenny Works <img className="dropdown-tringle" src={tringle_icon} alt="" border="0" /></NavLink>
										<div className={this.state.isActiveHiW ? 'menu-show dropdown-menu': 'dropdown-menu'}>
											<a href="penny_auctions_guide.html">Penny Auctions Guide</a>
											<a href="seat_auctions_guide.html">Seat Auctions Guide</a>
											<a href="unique_auctions_guide.html">Unique Auction Guide</a>
											<a href="ebay_auctions_guide.html">Ebay Auction Guide</a>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div id="flash_msg" className="flash_msg main_error" style={{ display: 'none' }} >
						<div id="error_inner"  className="error_inner"></div>
					</div>
				</React.Fragment>
			</div>
		)
	}
}

export default Header;


