import React, { Component, Fragment } from 'react';

import support_img from '../Assets/images/support_img.jpg';
import happy_img from '../Assets/images/happy_img.jpg';
import ticke_support_img from '../Assets/images/ticke_support_img.jpg';
import logo_fot from '../Assets/images/logo_fot.png';

class Footer extends Component {
  render() {
    return (
      <div>
  <div className="btm_bnr">
    <div className="doc_width">
  
  <div className="block_box width_33">
  <div className="box_content">
  <a href="">
  <div className="content-overlay content-overlay-blue"></div>
  <img className="content-image" src={support_img} />
  <div className="content-details fadeIn-top">
  <h3>Support 24/7</h3>
  <p> Contact us in any moment, you can create a support ticket or simply talk with us by the live chat. If live chat it is offline, create a support ticket or send us a email.</p>
  </div>
  </a>
  </div>
  </div>
  
  <div className="block_box width_33">
  <div className="box_content">
  <a href="">
  <div className="content-overlay content-overlay-green"></div>
  <img className="content-image" src={happy_img} />
  <div className="content-details fadeIn-top">
  <h3>Change your won item for Cash </h3>
  <p> On propennyauction you can choose, want the product auction that you won or prefer the product 
    value in cash? ... You dedice what you want after pay the won auction.</p>
  </div>
  </a>
  </div>
  </div>
  
  <div className="block_box width_33">
  <div className="box_content">
  <a href="">
  <div className="content-overlay content-overlay-purpule"></div>
  <img className="content-image" src={ticke_support_img} />
  <div className="content-details fadeIn-top">
  <h3>Multiple auctions types</h3>
  <p> Into the Auctions world propennyauction everything its possible, we have a lot of diferent types of auctions, and we work every day to give your more and more auctions types. </p>
  </div>
  </a>
  </div>
  </div>
  </div>
  </div>
   
  
   
  <div className="footerbg">
  
  <div className="fot_invite">
  <div className="doc_width">
  <span><strong>Invite Your Friends</strong> and get free bids.</span>
  <a className="btn_new  animated-btn animated-1" href="/invite-friend">
  Invite Friends</a>
  </div>
  </div>
  
  
  <div className="doc_width">
  
  <div className="footerlink">
  
  
  <ul className="about_ul">
  <p className="logo_footer"><img src={logo_fot} /></p>
  <p className="about_p">
  Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
  Lorem Ipsum has been the industry's standard dummy text ever since the 
  when an unknown printer took a galley of type 
  </p>
  </ul>
  
  
  
  <ul>
  <h2>Quick Links</h2>
   <li><a href="signup">Register now</a></li>
   <li><a href="auction">Live Auctions</a></li>
   <li><a href="closed">Cloesd Auctions</a></li> 
  </ul>
  
  
       
      
        <ul>
          <h2>About US</h2>
          <li> <a href="about">About us</a></li>
          <li><a href="privacy" target="_self">Privacy Policy</a></li>
          <li><a href="terms" target="_self">Terms and Conditions</a></li>
      
          </ul>
      
    
        <ul>
          <h2>Support</h2>
        <li><a href="help.html" target="_self">Help</a></li>
      <li><a href="how_it_works.html" target="_self">How it work  </a></li>
          <li><a href="contact.html">Contact</a></li>
      </ul>
          
        <ul className="follow">
          <h2>follow us on:</h2>
      <li className="social_icon f_icon"><a href="https://www.facebook.com/propennyauction">&nbsp;</a></li>
  <li className="social_icon t_icon"><a href="https://twitter.com/propennyauction">&nbsp;</a></li>
  <li className="social_icon y_icon"><a href="https://www.youtube.com/propennyauction">&nbsp;</a></li>
  <li className="social_icon g_icon"><a href="https://instagram.com/propennyauction">&nbsp;</a></li>		
      </ul> 
      
          
  </div>
     
  </div>
  </div>
      </div>
    )
  }
}

export default Footer

