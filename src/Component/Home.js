import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import '../Assets/css/animate.css';
// import '../Assets/css/style -final.css';
import '../Assets/css/style.css';
// import '../Assets/css/wheel.css';

import '../Assets/js/custom.js';
// import '../Assets/js/slide.js';
// import '../Assets/js/ui.js';
// import '../Assets/js/wow.js';

import user_icon from '../Assets/images/user_icon.svg';


import Header from '../CommonComponent/Header';
import Footer from '../CommonComponent/Footer';
import Steps from '../CommonComponent/Steps';

class Home extends Component {
    constructor() {
        super();
        this.state = {
            items: []
        }
    }

    componentDidMount() {
		fetch(`http://dev.propennyauction.com/api/index.php`, {
			method: "GET",
		}).then(response => response.json().then(data => ({ status: response.status, body: data })))
			.then(data => {
                if(data.status === 200) {
                    this.setState({
                        items: data.body.data
                    })
                }
			})
	}

    secondsToTime = (secs, cs) => {

        console.log("sec", secs, "cs", cs)
		if (cs === 1 || cs == 1 || cs == '1') {
			return 'SOLD';
		} if (secs < 0 && cs !== 1) {
			return '00:00:00 ';
		}
		let hours = Math.floor(secs / (60 * 60));
		if (hours < 10) hours = '0' + hours;

		let divisor_for_minutes = secs % (60 * 60);
		let minutes = Math.floor(divisor_for_minutes / 60);
		if (minutes < 10) minutes = '0' + minutes;

		let divisor_for_seconds = divisor_for_minutes % 60;
		let seconds = Math.ceil(divisor_for_seconds);
		if (seconds < 10) seconds = '0' + seconds;

		let obj = hours + ':' + minutes + ':' + seconds;
		return obj;
	}

    render() {
        return (
            <div>
                <Header />
                <Steps />
                <div className="step_titel mar_b_0">
                    <div className="doc_width">
                        <h1>Live Auctions</h1>             
                    </div>		
                </div>
                <div className="auction_type">
                    <ul>
                        <li className="penny" title="Penny Auction">Penny Auction <span></span></li>
                        <li className="seat" title="Seat Auction">Seat Auction <span></span></li>
                        <li className="unique" title="Unique Auction">Unique Auction <span></span></li>
                        <li className="ebay" title="Ebay Auction">Ebay Auction <span></span></li>
                        <li className="live" title="Live Auction">Live Auction <span></span></li>
                    </ul>
                </div>
                <div className='doc_width'>
                    <ul className='horizontal-bid-list'>
                        {this.state.items.map((auction) => {
                            return (
                                <li
                                    id={`auction-${auction.id}`}
                                    key={auction.id}
                                    className={`auction-item auction_${auction.id}`}
                                    rel={`auction_${auction.id}`}
                                    title={auction.title}
                                >
                                <i className='live_icon'>
                                    <span></span>
                                </i>
                                <font></font>
                                <font></font>
                                <font></font>
                                <font></font>
                                <h3>
                                    <a href='auction_detail.html'> {auction.title} </a>
                                </h3>
                                <div className='thumb clearfix'>
                                    <NavLink to={`/auctions/view/${auction.id}`}>
                                        <img src={auction.img} className='img_type_3' />
                                    </NavLink>
                                </div>
                                <span
                                    id={`timer_${auction.id}`}
                                    className='timer countdown'
                                    title='2020-08-25 01:53:37'
                                >
                                <span id={`second_left_count_${auction.id}`}>
                                    {this.secondsToTime(auction.second_left, auction.cs)}
                                </span>
                                </span>
                                    <div className='row row_username'>
                                        <span className='username_icon'>
                                            <img src={user_icon} alt='' />
                                        </span>
                                        <label className='username_highlight'>
                                            <span
                                                className='username bid-bidder'
                                                id={`bid_bidder_${auction.id}`}
                                            >
                                                {auction.username_leader}{' '}
                                            </span>
                                        </label>
                                        <span className='username_icon'>
                                            <img src={user_icon} alt='' />
                                        </span>
                                    </div>
                                    <div className='row row_price'>
                                        <div className='padd_lr_20'>
                                            <div className='left_auc_content f_l'>
                                                <div className='price_arrow'>
                                                    <div id='pricear'>
                                                        <span className='arrow1 arrow_up ar1'></span>
                                                        <span className='arrow1 arrow_down ar1'></span>
                                                    </div>
                                                </div>
                                                <span
                                                    className='bid-price'
                                                    id={`bid_price_${auction.id}`}
                                                >
                                                    ${auction.price}{' '}
                                                </span>
                                            </div>
                                            <div className='left_auc_content f_r text-right'>
                                                <span className='valu_price'>
                                                    <span>Value</span>
                                                    <span>$1.53</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='auc_btm_content blink'>
                                        <div className='bidbtn bid-button'>
                                            <a href='login' className='b-login-new'>
                                                {' '}
                                                <span>
                                                    Register or login to bid<span> </span>
                                                </span>
                                            </a>
                                        </div>
                                        <div className='btn_view hide'>
                                            <NavLink to={`/auctions/view/${auction.id}`}>View</NavLink>
                                        </div>
                                    </div>
                                </li>
                            );
                        })}
                    </ul>
                </div>
                <Footer />
            </div>
        )
    }
}

export default Home
